package vistadatos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;

import modelo.Modelo;
import modelo.Persona;

/**
 *
 * @author paco
 */
public class Fichero implements Modelo {

    private File fs;
    private int id = 0;
    private static String Fichero = "personas.csv";

    private int calculaid() {
        int idmax = 0;

        if (fs.exists()) {  //si el fichero existe leerlo con fileReader

            try {

                FileReader fr = new FileReader(fs);
                BufferedReader br = new BufferedReader(fr);
                Persona persona;
                String linea;
                int id_ = 0;

                linea = br.readLine(); //linea es la variable donde se guardar lo recogido en el buffer
                while (linea != null) {
                    persona = extraePersona(linea);
                    // id_ = Integer.parseInt(persona.getId()); 
                    if (id_ > idmax) {
                        idmax = id_;
                    }
                    linea = br.readLine();
                }

                fr.close();

            } catch (FileNotFoundException ex) {
            } catch (IOException ex) {
            }

        }
        return idmax + 1;

    }

    public Fichero() throws IOException {
        fs = new File(Fichero);
        id = calculaid();
    }

    @Override
    public void create(Persona persona) {

        FileWriter fw = null;
        try {
            fw = new FileWriter(fs, true);
            persona.setIdpersona(id + "");
            grabarPersona(persona, fw);
            fw.close();
        } catch (IOException ex) {
        }
        id++;
    }

    private Persona extraePersona(String linea) {

        Persona persona;
        StringTokenizer str = new StringTokenizer(linea, ";");

        String idpersona = str.nextToken();
        String usuario = str.nextToken();
        String password = str.nextToken();
        //int edad = Integer.parseInt(edad_);
        String iddepartamento = str.nextToken();

        persona = new Persona(idpersona, usuario, password, iddepartamento);
        return persona;
    }

    public Persona read(Persona persona) {

        Boolean encontrado = false;

        try {
            FileReader fr = new FileReader(fs);
            BufferedReader br = new BufferedReader(fr);

            String linea;

            linea = br.readLine();
            while (linea != null) {
                persona = extraePersona(linea);
                if (idpersona.equals(persona.getIdpersona())) {
                    encontrado = true;
                    break;
                }

                linea = br.readLine();
            }
            fr.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }

        if (!encontrado) {
            persona = null;
        }
        return persona;
    }

    public void update(Persona persona) {

        try {
            File temp = new File("temp.txt");
            FileWriter fw = new FileWriter(temp);
            FileReader fr = new FileReader(fs);

            BufferedReader br = new BufferedReader(fr);
            Persona p;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                p = extraePersona(linea);
                if (p.getIdpersona().equals(persona.getIdpersona())) {
                    grabarPersona(persona, fw);
                } else {
                    grabarPersona(p, fw);
                }
                linea = br.readLine();
            }
            fr.close();
            fw.close();

            fs.delete();
            temp.renameTo(fs);

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }

    }

    public void delete(Persona persona) {

        try {

            File temp = new File("temp.txt");

            FileWriter fw = new FileWriter(temp);
            FileReader fr = new FileReader(fs);

            BufferedReader br = new BufferedReader(fr);
            Persona p;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                p = extraePersona(linea);
                if (!p.getIdpersona().equals(persona.getIdpersona())) {
                    grabarPersona(p, fw);
                }
                linea = br.readLine();
            }
            fr.close();
            fw.close();

            fs.delete();
            temp.renameTo(fs);

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }

    }

    private void grabarPersona(Persona persona, FileWriter fw) throws IOException {
        fw.write(persona.getIdpersona());
        fw.write(";");
        fw.write(persona.getUsuario());
        fw.write(";");
        fw.write(persona.getPassword());
        fw.write(";");
        fw.write(persona.getDepartamento().getIddepartamento());
        fw.write("\r\n");
    }

}
