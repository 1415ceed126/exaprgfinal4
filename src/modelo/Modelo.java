/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public interface Modelo {

    public void create(Persona persona); // Crea un alumno nuevo

    public void update(Persona persona); // Actuzaliza el alumno.

    public void delete(Persona persona);  // Borrar el alunno con el id dado

    public Persona read(Persona persona);  // Obtiene un alumno

}
