
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import modelo.Departamento;
import modelo.Persona;
import vistadatos.Bd;
import vistadatos.Db4o;
import vistapantalla.VistaPrincipal;


class ControladorPrincipal implements ActionListener {

  private VistaPrincipal vista;
  private ArrayList<Persona> personas = new ArrayList<>();
  private int intentos = 0;
  private ControladorPersona ControladorPersona;
  
   // Recoge lo que le pasa el Main a través del constructor
    // Guarda lo que le pasan en el Main variable vista y personas del controlador
    // principal
  
    ControladorPrincipal(VistaPrincipal vista, ArrayList<Persona> personas) {
    this.vista = vista;
    this.personas = personas;

    inicializabotones();
    inicializaobjetos();
    grabarPersonas();
    visualizar();
  }

  public void visualizar() {
    vista.setVisible(true);
  }

  public void inicializabotones() {
    vista.getBt1().setActionCommand("Entrar");
    vista.getBt1().addActionListener(this);
    vista.getBt1().setText("Entrar");

    vista.getBt2().setActionCommand("Salir");
    vista.getBt2().addActionListener(this);
    vista.getBt2().setText("Salir");
  }

  @Override
  public void actionPerformed(ActionEvent ae) {
    String comando = ae.getActionCommand();

    switch (comando) {
      case "Entrar":
        entrar();
        break;
      case "Salir":
        salir();
        break;
      case "SalirVistaPersona":
        ControladorPersona.SalirVistaPersona();
        break;
      case "GrabarVistaPersona":
        ControladorPersona.GrabarVistaPersona();
        break;
    } // swith
  }

  public void entrar() {

    Boolean usuarioCorrecto = false;

    String usuario = vista.getJtf1().getText();
    String password = vista.getJtf2().getText();
    Persona personabuscada = new Persona(null, usuario, password, null);
    Persona persona;

    persona = buscarPersona(personabuscada);

    if (persona != null) {
      ControladorPersona = new ControladorPersona(persona, vista, this);
      ControladorPersona.visualizar();
    } else { // No es correcto.
      intentos++;
      vista.getJl3().setText("Error: Usuario Incorrecto. Intento:" + intentos);
    }

    if (intentos >= 3 && !usuarioCorrecto) {
      salir();
    }
  }

  public void salir() {
    vista.dispose();
  }

  public void SalirVistaPersona() {
    vista.dispose();
  }

  public void inicializaobjetos() {

    Departamento d1 = new Departamento("d1", "Inf");

    Persona p1 = new Persona("p1", "admin", "123", d1);
    Persona p2 = new Persona("p2", "paco", "123", d1);

    personas.add(p1);
    personas.add(p2);
  }

  public Persona buscarPersona(Persona personabuscada) {

    Iterator it = personas.iterator();
    while (it.hasNext()) {
      Persona persona = (Persona) it.next();
      if (personabuscada.getUsuario().equals(persona.getUsuario()) && 
              personabuscada.getPassword().equals(persona.getPassword())) {
        return persona;
      }
    }
    return null;
  }

  public void grabarPersonas() {

    if (personas.size() == 0) {
      return;
    }

    Bd bd = new Db4o();
    bd.open();
    Iterator it = personas.iterator();
    while (it.hasNext()) {
      Persona persona = (Persona) it.next();
      bd.escribir(persona);
    }
    bd.close();
  }
}
